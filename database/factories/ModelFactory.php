<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Beer::class, function(Faker\Generator $faker) {

    return [
        'title' => $faker->realText(15),
        'beer_type' => $faker->realText(15),
        'abv' => $faker->randomFloat(2, 4.0, 20.0),
        'ibu' => $faker->numberBetween(0, 100),
        'description' => $faker->text(400),
    ];

});

$factory->define(App\Event::class, function(Faker\Generator $faker) {

    $title = $faker->sentence();
    $dateTime = $faker->dateTimeBetween('now', '+2 years');
    $date = $dateTime->format('Y-m-d');
    $slug = str_slug($date.' '.$title);

    return [
        'title' => $title,
        'date' => $dateTime,
        'slug' => $slug,
        'start_time' => $faker->time(),
        'end_time' => $faker->time(),
        'price' => '$'.$faker->randomFloat(2, 0, 20),
        'body' => '<p>'.implode('</p><p>', $faker->paragraphs(4)).'</p>',
    ];

});