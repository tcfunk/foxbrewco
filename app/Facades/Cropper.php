<?php

namespace App\Facades;

// use Illuminate\Support\Facade;
use Illuminate\Support\Facades\Storage;

class Cropper
{
    /**
     * Crop provided image based on provided dimensions
     *
     * @return string
     */
    public static function crop($source, $cropData)
	{
		// Get image data
		$pinfo = pathinfo($source);
		$ext = $pinfo['extension'];

		// Get path information for use later
		$url = Storage::get($source);
		$img = imagecreatefromstring($url);

		// Create cropped version
		$cropped = imagecrop($img, $cropData);

		// Create file data from cropped image
		ob_start();
		if ($ext == 'png') {
			imagepng($cropped);
		} else if ($ext == 'jpg' || $ext == 'jpeg') {
			imagejpeg($cropped);
		}
		$fileData = ob_get_clean();

		// Store new image file
		$croppedUrl = $pinfo['dirname'].'/'.$pinfo['filename'].'-c.'.$ext;

        return Storage::put($croppedUrl, $fileData) ? $croppedUrl : null;
	}
}

