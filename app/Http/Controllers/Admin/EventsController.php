<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use \App\Event;
use \App\EventType;
use \App\Facades\Cropper;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('date', 'desc')
                ->paginate(20);
        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = new Event;
        $event->venue = '';
        $event->address = '';
        return view('admin.events.create', compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event(request([
            'title',
            'date',
            'start_time',
            'end_time',
            'price',
            'body',
            'photo',
            'venue',
            'address',
        ]));

        if (!empty(request()->file('photo'))) {
            $event->photo = request()->file('photo')->store('public');
        }

        $event->slug = str_slug($event->date.' '.$event->title);

        if ($event->save()) {
            return redirect(route('events.index'));
        }
        else {
            return view('admin.events.create', compact('event'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        return view('admin.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        return view('admin.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);
        $event->update(request([
            'title',
            'date',
            'start_time',
            'end_time',
            'price',
            'body',
            'venue',
            'address',
        ]));

        if (!empty(request()->file('photo'))) {
            $event->photo = request()->file('photo')->store('public');
        }

        if ($event->save()) {
            return redirect(route('events.index'));
        }
        else {
            return view('admin.events.edit', compact('event'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::destroy($id);
        return redirect(route('events.index'));
    }

	public function upload()
	{
		$path = request()->file('image')->store('public/events');
		$url = Storage::url($path);

		return response()->json([
			'path' => $path,
			'url' => $url,
		]);
	}
}
