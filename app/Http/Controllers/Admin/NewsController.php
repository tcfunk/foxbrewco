<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \App\Article;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new Article;
        return view('admin.articles.create', compact('article'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article(request([
            'title',
            'date',
            'body',
            'photo',
        ]));

        if (!empty(request()->file('photo'))) {
            $article->photo = request()->file('photo')->store('public');
        }

        $article->slug = str_slug($article->date->format('Y-m-d').' '.$article->title);

        if ($article->save()) {
            return redirect(route('articles.index'));
        }
        else {
            return view('admin.articles.create', compact('article'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('articles.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        return view('admin.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->update(request([
            'title',
            'date',
            'body',
            'photo',
        ]));

        if (!empty(request()->file('photo'))) {
            $article->photo = request()->file('photo')->store('public');
        }

        if ($article->save()) {
            return redirect(route('articles.index'));
        }
        else {
            return view('admin.articles.edit', compact('article'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
        return redirect(route('articles.index'));
    }
}
