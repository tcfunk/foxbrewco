<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use \App\Beer;
use \App\BeerType;
use \App\Facades\Cropper;

class BeersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beers = Beer::orderBy('title')->get();
        return view('admin.beers.index', compact('beers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $beer = new Beer;
        $types = BeerType::orderBy('title')->get();
        return view('admin.beers.create', compact('beer', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      /* $beer = new Beer(request(['title', 'beer_type_id', 'abv', 'ibu', 'description', 'og_image']));*/
        $beer = new Beer(request(['title', 'beer_type', 'abv', 'ibu', 'description', 'og_image']));

        if ($beer->save()) {
            return redirect(route('beers.index'));
        }
        else {
            $types = BeerType::orderBy('title')->get();
            return view('admin.beers.create', compact('beer', 'types'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $beer = Beer::find($id);
        return view('admin.beers.show', compact('beer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $beer = Beer::find($id);
        $types = BeerType::orderBy('title')->get();
		$beer->cropdata = json_encode(array(
			'x' => 0,
			'y' => 0,
			'width' => 800,
			'height' => 600,
		));
        return view('admin.beers.edit', compact('beer', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $beer = Beer::find($id);
        $beer->update(request(['title', 'beer_type_id', 'abv', 'ibu', 'description', 'og_image']));

		// Create cropped version of beer image
		$croppedUrl = Cropper::crop($beer->og_image, [
			'x'      => request('crop_x'),
			'y'      => request('crop_y'),
			'width'  => request('crop_w'),
			'height' => request('crop_h'),
		]);

		// Update beer image
		$beer->image = $croppedUrl;

        if ($beer->save()) {
            return redirect(route('beers.index'));
        }
        else {
            $types = BeerType::orderBy('title')->get();
            return view('admin.beers.edit', compact('beer', 'types'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Beer::destroy($id);
        return redirect(route('beers.index'));
    }

	public function upload()
	{
		$path = request()->file('image')->store('public/beers');
		$url = Storage::url($path);

		return response()->json([
			'path' => $path,
			'url' => $url,
		]);
	}
}
