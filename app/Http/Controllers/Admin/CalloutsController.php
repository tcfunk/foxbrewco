<?php

namespace App\Http\Controllers\Admin;

use App\Callout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalloutsController extends Controller
{
    protected $icons = [
        'glyphicon-envelope'   => 'Envelope',
        'glyphicon-film'       => 'Film Strip',
        'glyphicon-fire'       => 'Fire',
        'glyphicon-globe'      => 'Globe',
        'glyphicon-grain'      => 'Grain',
        'glyphicon-heart'      => 'Heart',
        'glyphicon-map-marker' => 'Map Marker',
        'glyphicon-music'      => 'Music Note',
        'glyphicon-star'       => 'Star',
        'glyphicon-tag'        => 'Tag',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $callouts = Callout::all();
        return view('admin.callouts.index', compact('callouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $callout = new Callout;
        $icons = $this->icons;
        return view('admin.callouts.create', compact('callout', 'icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $callout = new Callout;
        $callout->fill(request([
            'title',
            'link',
            'active',
            'icon',
        ]));

        $callout->weight = 1;
        $callout->image = request()->file('image')->store('public');

        if ($callout->save()) {
            return redirect(route('callouts.index'));
        } else {
            return view('admin.callouts.create', compact('callout'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Callout  $callout
     * @return \Illuminate\Http\Response
     */
    public function show(Callout $callout)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Callout  $callout
     * @return \Illuminate\Http\Response
     */
    public function edit(Callout $callout)
    {
        $icons = $this->icons;
        return view('admin.callouts.edit', compact('callout', 'icons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Callout  $callout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Callout $callout)
    {
        $callout->fill(request([
            'title',
            'link',
            'active',
        ]));

        $callout->weight = 1;
        if (!empty(request()->file('image')))
        {
            $callout->image = request()->file('image')->store('public');
        }

        if ($callout->save()) {
            return redirect(route('callouts.index'));
        } else {
            return view('admin.callouts.edit', compact('callout'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Callout  $callout
     * @return \Illuminate\Http\Response
     */
    public function destroy(Callout $callout)
    {
        $callout->delete();
        return redirect(route('callouts.index'));
    }
}
