<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class Controller extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.index');
    }
}
