<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \App\Contact;

class ContactsController extends Controller
{
	public function index()
	{
		$contacts = Contact::all();

		return view('admin.contacts.index', compact('contacts'));
	}

    public function show(Contact $contact)
    { 
        return view('admin.contacts.show', compact('contact'));
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();
        return redirect('ContactsController@index');
    }
}
