<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \App\BeerType;

class BeerTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = BeerType::orderBy('title')->get();
        return view('admin.beer-types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = new BeerType;
        return view('admin.beer-types.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = new BeerType(request(['title']));
        $type->save();

        if ($type->save()) {
            return redirect(route('beer-types.index'));
        }
        else {
            return view('admin.beer-types.create', compact('type'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = BeerType::find($id);
        return view('admin.beer-types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = BeerType::find($id);
        $type->update(request(['title']));

        if ($type->save()) {
            return redirect(route('beer-types.index'));
        }
        else {
            return view('admin.beer-types.edit', compact('type'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BeerType::destroy($id);
        return redirect(route('beer-types.index'));
    }
}
