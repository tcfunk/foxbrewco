<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use \App\Beer;
use \App\Http\Controllers\Controller;


class BeersController extends Controller
{
	/**
	 * Return a JSON array of the beers data.
	 *
	 * @return string
	 */
	public function index()
	{
		$beers = Beer::orderBy('title')->get();
		foreach ($beers as &$beer) {
			$beer->realImage = asset(Storage::url($beer->image));
		}
		return $beers->jsonSerialize();
	}
}