<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeerType extends Model
{
	protected $fillable = ['title'];

    public function beers()
    {
        return $this->hasMany('App\Beer', 'beer_type_id');
    }
}
