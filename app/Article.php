<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title',
        'date',
        'start_time',
        'end_time',
        'price',
        'body',
        'photo',
        'venue',
        'address',
    ];

    protected $dates = [
        'date',
        'updated_at',
        'created_at',
    ];
}
