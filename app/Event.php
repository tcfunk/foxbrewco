<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    protected $fillable = [
        'title',
        'date',
        'start_time',
        'end_time',
        'price',
        'body',
        'photo',
        'venue',
        'address',
    ];

    protected $dates = [
        'date',
        'updated_at',
        'created_at',
    ];

    public function startTime()
    {
        return Carbon::parse($this->start_time)->format('g:i A');
    }

    public function endTime()
    {
        return Carbon::parse($this->end_time)->format('g:i A');
    }

    public function venue()
    {
        return empty($this->venue) ? 'Fox Brewing' : $this->venue;
    }

    public function addressLink()
    {
        $venue = empty($this->venue) ? 'Fox Brewing Company' : $this->venue;
        $address = empty($this->address) ? '103 S 11th Street West Des Moines, IA 50265' : $this->address;
        $full = $venue . ' ' . $address;
        return 'https://www.google.com/maps/search/' . urlencode($full);
    }
}

