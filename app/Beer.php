<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beer extends Model
{
	protected $fillable = ['title', 'beer_type', 'abv', 'ibu', 'description', 'og_image', 'image', 'cropdata'];

    // public function type()
    // {
    //     return $this->belongsTo('App\BeerType', 'beer_type_id');
    // }
}
