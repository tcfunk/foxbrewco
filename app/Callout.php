<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Callout extends Model
{
    protected $fillable = [
        'title',
        'link',
        'active',
        'icon',
    ];
}
