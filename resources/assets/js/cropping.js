require('cropper/dist/cropper.min.js');
require('blueimp-file-upload/js/vendor/jquery.ui.widget.js');
require('blueimp-file-upload/js/jquery.iframe-transport.js');
require('blueimp-file-upload/js/jquery.fileupload.js');

$(function() {

    function makeCropper(target) {
        var $target = $(target);

        var data = {};
        if ($target.data('crop') !== undefined) {
            data = $target.data('crop');
        }

        data.top = 0;
        data.left = 0;

        data.aspectRatio = 4/3;
        data.zoomable = false;
        data.draggable = false;
        data.crop = function(e) {
            $('#crop_x').val(e.x);
            $('#crop_y').val(e.y);
            $('#crop_w').val(e.width);
            $('#crop_h').val(e.height);
        };

        $target.cropper(data);
    }

    $('#image').fileupload({
        dataType: 'json',
        done: function(e, data) {
            $('#og_image').val(data.result.path);
            $('#img-crop').cropper('replace', data.result.url);
        }
    });

    makeCropper('#img-crop');
});

