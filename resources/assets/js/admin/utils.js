'use strict';

(function($) {

    $('form.confirm').on('submit', function() {
        return confirm('Are you sure?');
    });

})(jQuery);
