// require('vue');
window.Vue = require('vue');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

new Vue({
	el: '#beers-container',

	data: {
		types: [
			'All',
		],
		beers: [],
		filterBy: 'All'
	},

	computed: {
		filteredBeers: function() {
			if (this.filterBy === 'All') return this.beers;

			var filteredResults = [];
			for (var i = 0; i < this.beers.length; i++) {
				if (this.beers[i].beer_type === this.filterBy) {
					filteredResults.push(this.beers[i]);
				}
			}

			return filteredResults;
		}
	},

	methods: {
		setFilter: function(val) {
			this.filterBy = val;
		}
	},

	mounted: function() {
		axios({
			method: 'get',
			url: '/api/beers'
		})
			.then((response) => {
				this.beers = response.data;

				// Identify unique beer types
				this.types = ['All'];
				for (var i = 0; i < response.data.length; i++) {
					var item = response.data[i];
					console.log(item.beer_type);
					if (this.types.indexOf(item.beer_type) < 0) {
						this.types.push(item.beer_type);
					}
				}
			});
	},

	template: `
<div class="container">

	<ul class="nav nav-pills">
		<li role="presentation" v-for="type in types" :class="{active: filterBy === type}"><a href='' @click.prevent="setFilter(type)">{{ type }}</a></li>
	</ul>

	<div id="beer-list">
		<div class="card col-md-4 col-sm-6" v-for="beer in filteredBeers">
			<figure>
				<img v-if="beer.image" :alt="beer.title" :src="beer.realImage" />
				<img v-if="!beer.image" :alt="beer.title" src="http://placehold.it/800x600" />
			</figure>

			<h3>{{ beer.title }} <small>{{ beer.beer_type }}</small></h3>
			<p>ABV: {{ beer.abv }} IBU: {{ beer.ibu }}</p>
			<hr/>
			<p>{{ beer.description }}</p>
		</div>
	</div>

	<p v-show="beers.length == 0">Pouring beers...</p>

</div>
`
});
