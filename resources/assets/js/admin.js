
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

$(function() {
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.timepicker').timepicker({
        step: 15,
        timeFormat: 'g:i A'
    });

    var toolbarOptions = [
        [{ 'header': [3, 4, 5, 6, false] }],
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        ['link'],

        ['clean']                                         // remove formatting button
    ];

    if ($('#quill-editor').length) {
        var quill = new Quill('#quill-editor', {
            theme: 'snow',
            modules: {
                toolbar: toolbarOptions
            }
        });

        quill.on('text-change', function(delta, oldDelta, source) {
            var target = $('#quill-editor').data('field');
            $('#'+target).val($('#quill-editor').find('.ql-editor').html());
        });
    }

    $('#icon').on('change', function() {
        var klass = $(this).val();
        if (klass != '') {
            $('#icon-preview').removeClass().addClass('glyphicon '+klass);
        } else {
            $('#icon-preview').removeClass();
        }
    });
});
