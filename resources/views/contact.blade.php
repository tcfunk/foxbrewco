@extends('layouts.main')

@section('title', 'Contact')

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>Contact</li>
@endsection

@section('content')
    <div class="container">
        <div class="card col-md-8 col-sm-12">
            <h1>Send Us A Message</h1>
            <hr/>

            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach

            <form class="contact-form" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label" for="name">Name</label>
                    <input class="form-control" id="name" name="name" type="text" value="{{ old('name') }}" required/>
                </div>

                <div class="form-group">
                    <label class="control-label" for="email">Email</label>
                    <input class="form-control" id="email" name="email" type="email" value="{{ old('email') }}" required/>
                </div>

                <div class="form-group">
                    <label class="control-label" for="phone">Phone</label>
                    <input class="form-control" id="phone" name="phone" type="tel" value="{{ old('phone') }}"/>
                </div>

                <div class="form-group">
                    <label class="control-label" for="message">Message</label>
                    <textarea class="form-control" id="message" cols="30" name="message" rows="10" required>{{ old('message') }}</textarea>
                </div>

                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
            </form>
        </div>

        @include('shared.contact-sidenav')
    </div>
@endsection
