@extends('layouts.admin')

@section('title', $slide->title)

@section('breadcrumb')
    <li><a href="{{ route('slides.index') }}">Slides</a></li>
    <li>{{ $slide->title }}</li>
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('slides.update', $slide) }}" enctype="multipart/form-data">
        {{ method_field('PUT') }}

        <div class="row">

            <div class="col-sm-12">
                @include('admin.slides._fields')
            </div>

            <div class="col-sm-12">
                <button class="btn btn-default">Update</button>
            </div>

        </div>

    </form>
</div>
@endsection
