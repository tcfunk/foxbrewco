@extends('layouts.admin')

@section('title', 'Homepage Slides')

@section('breadcrumb')
    <li>Slides</li>
@endsection

@section('content')
    <div class="container">

        <a class="btn btn-success pull-right" href="{{ route('slides.create') }}"><i class="glyphicon glyphicon-plus"></i> Add Slide</a>

        <table class="table table-hover table-striped">
            <colgroup>
                <col class="title"/>
                <col class="manage"/>
            </colgroup>

            <thead>
                <th>Title</th>
                <th width="15%">Manage</th>
            </thead>

            <tbody>
                @foreach ($slides as $row)
                    <tr>
                        <td>{{ $row->title }}</td>
                        <td>
                            <a class="btn btn-default btn-xs" href="{{ route('slides.edit', ['type' => $row]) }}">Edit</a>

                            <form method="POST" action="{{ route('slides.destroy', ['type' => $row]) }}" class="confirm inline-block">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
