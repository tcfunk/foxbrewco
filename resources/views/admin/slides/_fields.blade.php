{{ csrf_field() }}


<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>
                <input name="active" type="checkbox" value="1" {{ $slide->active ? 'checked=checked':'' }}/>
                Active
            </label>
        </div>

        <div class="form-group">
            <label for="image">Image (suggested minimum: 1600px &times; 400px)</label>
            <input type="file" name="image" id="image" />
            @if(!empty($slide->image))
                <img class="img-responsive" alt="" src="{{ asset(Storage::url($slide->image)) }}"/>
            @endif
        </div>
    </div>
</div>


<div class="row">
    <div class="form-group col-sm-6">
        <label for="title">Overlay text</label>
        <input id="title" name="title" type="text" value="{{ $slide->title }}" class="form-control"/>
    </div>

    <div class="form-group col-sm-6">
        <label for="link">Link <span class="required">*</span></label>
        <input id="link" class="form-control" name="link" type="text" value="{{ $slide->link }}"/>
    </div>
</div>
