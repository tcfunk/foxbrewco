@extends('layouts.admin')

@section('title', 'New Slide')

@section('breadcrumb')
    <li><a href="{{ route('slides.index') }}">Slides</a></li>
    <li>New Slide</li>
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('slides.store') }}" enctype="multipart/form-data">
        <div class="row">

            <div class="col-sm-12">
                @include('admin.slides._fields')
            </div>

            <div class="col-sm-12">
                <button class="btn btn-default">Create</button>
            </div>

        </div>

    </form>
</div>
@endsection
