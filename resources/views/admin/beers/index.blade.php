@extends('layouts.admin')

@section('title', 'Beers')

@section('breadcrumb')
    <li>Beers</li>
@endsection

@section('content')
    <div class="container">

        <a class="btn btn-success pull-right" href="{{ route('beers.create') }}"><i class="glyphicon glyphicon-plus"></i> Add Beer</a>

        <table class="table table-hover table-striped">
            <thead>
                <th>Title</th>
                <th>Type</th>
                <th>ABV</th>
                <th>IBU</th>
                <th width="15%">Manage</th>
            </thead>
            <tbody>
                @foreach ($beers as $row)
                    <tr>
                        <td>{{ $row->title }}</td>
                        <td>{{ $row->title }}</td>
                        <td>{{ $row->abv }}</td>
                        <td>{{ $row->ibu }}</td>
                        <td>
                            <a class="btn btn-default btn-xs" href="{{ route('beers.edit', ['type' => $row]) }}">Edit</a>

                            <form method="POST" action="{{ route('beers.destroy', ['type' => $row]) }}" class="confirm inline-block">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
