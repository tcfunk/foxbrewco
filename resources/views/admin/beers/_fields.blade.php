{{ csrf_field() }}

<input type="hidden" name="og_image" id="og_image" value="{{ $beer->og_image }}" />

<input type="hidden" name="crop_x" id="crop_x" value="" />
<input type="hidden" name="crop_y" id="crop_y" value="" />
<input type="hidden" name="crop_w" id="crop_w" value="" />
<input type="hidden" name="crop_h" id="crop_h" value="" />

<div class="row">
	<div class="form-group col-sm-6">
		<label for="title">Title</label>
		<input class="form-control" id="title" name="title" type="text" value="{{ $beer->title }}"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="beer_type">Type</label>
		<input class="form-control" id="beer_type" name="beer_type" type="text" value="{{ $beer->beer_type }}"/>
		{{-- <label for="beer_type_id">Type</label> --}}
		{{-- <select id="beer_type_id" name="beer_type_id" class="form-control"> --}}
		{{-- <option>select one</option> --}}
		{{-- @foreach ($types as $type) --}}
		{{-- <option value="{{ $type->id }} " {{ $type->id==$beer->beer_type_id?'selected':'' }}>{{ $type->title }}</option> --}}
		{{-- @endforeach --}}
		{{-- </select> --}}
	</div>
</div>

<div class="row">
	<div class="form-group col-sm-6">
		<label for="abv">ABV</label>
		<input class="form-control" id="abv" name="abv" type="number" step="0.1" value="{{ $beer->abv }}"/>
	</div>

	<div class="form-group col-sm-6">
		<label for="abv">IBU</label>
		<input class="form-control" id="ibu" name="ibu" type="number" value="{{ $beer->ibu }}"/>
	</div>
</div>

<div class="form-group">
	<label for="description">Description</label>
	<textarea cols="30" id="description" class="form-control" name="description" rows="5">{{ $beer->description }}</textarea>
</div>

