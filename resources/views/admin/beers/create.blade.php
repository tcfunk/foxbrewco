@extends('layouts.admin')

@section('title', 'New Beer')

@section('breadcrumb')
    <li><a href="{{ route('beers.index') }}">Beers</a></li>
    <li>New Beer</li>
@endsection

@section('content')
<div class="container">
	<div class="row">

		<div class="col-sm-8">
			<form method="POST" action="{{ route('beers.store') }}">
				@include('admin.beers._fields')
				<button class="btn btn-default">Create</button>
			</form>
		</div>

		<div class="col-sm-4">
			<form action="/admin/upload/beers" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="image">Image (800px &times; 600px)</label>
					<input type="file" name="image" id="image" />
				</div>

				<div class="form-group">
					<img id="img-crop" src="{{ asset('img/800x600.png') }}" alt="croppable" class="img-responsive" />
				</div>
			</form>
		</div>

	</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/cropping.css') }}" />
@endsection

@section('js')
<script src="{{ asset('js/cropping.js') }}"></script>
@endsection
