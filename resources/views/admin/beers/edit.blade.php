@extends('layouts.admin')

@section('title', $beer->title)

@section('breadcrumb')
    <li><a href="{{ route('beers.index') }}">Beers</a></li>
    <li>{{ $beer->title }}</li>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<form method="POST" action="{{ route('beers.update', ['beer' => $beer]) }}" enctype="multipart/form-data">
				{{ method_field('put') }}
				@include('admin.beers._fields')
				<button class="btn btn-default">Update</button>
			</form>
		</div>

		<form action="/admin/upload/beers" method="POST">
			{{ csrf_field() }}
			<div class="col-sm-4">
				<div class="form-group">
					<label for="image">Image (800px &times; 600px)</label>
					<input type="file" name="image" id="image" />
				</div>

				<div class="form-group">
					<img id="img-crop" src="{{ asset(Storage::url($beer->og_image)) }}" alt="croppable" class="img-responsive" {{ isset($beer->cropdata)&&!empty($beer->cropdata)?'data-crop='.$beer->cropdata:'' }} />
				</div>
			</div>
		</form>

	</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/cropping.css') }}" />
@endsection

@section('js')
<script src="{{ asset('js/cropping.js') }}"></script>
@endsection
