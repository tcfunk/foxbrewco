@extends('layouts.admin')

@section('title', 'Events')

@section('breadcrumb')
    @if($events->currentPage() == 1)
        <li>Events</li>
    @else
        <li><a href="{{ route('events.index') }}">Events</a></li>
    @endif
@endsection

@section('content')
    <div class="container">

        <a class="btn btn-success pull-right" href="{{ route('events.create') }}"><i class="glyphicon glyphicon-plus"></i> Add Event</a>

        <table class="table table-hover table-striped">
            <thead>
                <th>Title</th>
                <th>Date</th>
                <th>Venue</th>
                <th width="15%">Manage</th>
            </thead>

            <tbody>
                @foreach ($events as $row)
                    <tr>
                        <td>{{ $row->title }}</td>
                        <td>{{ $row->date }}</td>
                        <td>{{ $row->venue ? $row->venue : 'Fox Brewing' }}</td>
                        <td>
                            <a class="btn btn-default btn-xs" href="{{ route('events.edit', ['type' => $row]) }}">Edit</a>

                            <form method="POST" action="{{ route('events.destroy', ['type' => $row]) }}" class="confirm inline-block">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>


        {{ $events->links() }}


    </div>
@endsection
