@extends('layouts.admin')

@section('title', $event->title)

@section('breadcrumb')
    <li><a href="{{ route('events.index') }}">Events</a></li>
    <li>{{ $event->title }}</li>
@endsection

@section('content')
<div class="container">
			<form method="POST" action="{{ route('events.update', compact('event')) }}" enctype="multipart/form-data">
	<div class="row">

		<div class="col-sm-8">
				{{ method_field('put') }}
				@include('admin.events._fields')
				<button class="btn btn-default">Update</button>
		</div>

		<div class="col-sm-4">
				<div class="form-group">
					<label for="photo">Image (900px &times; 675px)</label>
					<input type="file" name="photo" id="photo" />
				</div>

        @if(!empty($event->photo))
        <div class="form-group">
					<img id="img-crop" src="{{ asset(Storage::url($event->photo)) }}" alt="croppable" class="img-responsive" />
				</div>
        @endif
		</div>

	</div>
			</form>
</div>
@endsection
