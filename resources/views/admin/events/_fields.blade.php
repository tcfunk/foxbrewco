{{ csrf_field() }}

<div class="row">
    <div class="form-group col-sm-8">
        <label for="title">Title</label>
        <input id="title" name="title" type="text" value="{{ $event->title }}" class="form-control"/>
    </div>

    <div class="form-group col-sm-4">
        <label for="date">Date</label>
        <input id="date" name="date" type="date" value="{{ $event->date }}" class="datepicker form-control"/>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-4">
        <label for="price">Price</label>
        <input id="price" class="form-control" name="price" type="text" value="{{ $event->price }}" />
    </div>

    <div class="form-group col-sm-4">
        <label for="start_time">Start Time</label>
        <input id="start_time" class="form-control timepicker" name="start_time" type="text" value="{{ $event->start_time }}" />
    </div>

    <div class="form-group col-sm-4">
        <label for="end_time">End Time</label>
        <input id="end_time" class="form-control timepicker" name="end_time" type="text" value="{{ $event->end_time }}"/>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-8">
        <label for="venue">Venue Name</label>
        <input id="venue" class="form-control" name="venue" type="text" value="{{ $event->venue }}"/>
        <span class="help-block">Leave blank for Fox Brewing</span>
    </div>

    <div class="form-group col-sm-12">
        <label for="address">Venue Address</label>
        <textarea id="address" class="form-control" name="address" rows="5">{{ $event->address }}</textarea>
        <span class="help-block">Leave blank for Fox Brewing address</span>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12">
        <label for="body">Description</label>
        <input id="body" name="body" type="hidden" value="{{ $event->body }}"/>
        <div id="toolbar"></div>
        <div id="quill-editor" data-field="body">{!! $event->body !!}</div>
    </div>
</div>
