@extends('layouts.admin')

@section('title', 'New Event')

@section('breadcrumb')
    <li><a href="{{ route('events.index') }}">Events</a></li>
    <li>Events</li>
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('events.store') }}" enctype="multipart/form-data">
        <div class="row">

            <div class="col-sm-8">
                @include('admin.events._fields')
                <button class="btn btn-default">Create</button>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Image (900px &times; 675px)</label>
                    <input type="file" name="photo" id="photo" />
                </div>
            </div>

        </div>

    </form>
</div>
@endsection
