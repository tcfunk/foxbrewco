@extends('layouts.admin')

@section('title', 'Beer Types')

@section('breadcrumb')
    <li>Beer Types</li>
@endsection

@section('content')
    <div class="container">

        <a class="btn btn-success pull-right" href="{{ route('beer-types.create') }}"><i class="glyphicon glyphicon-plus"></i> Add Beer Type</a>

        <table class="table table-hover table-striped">
            <thead>
                <th width="85%">Title</th>
                <th width="15%">Manage</th>
            </thead>
            <tbody>
                @foreach ($types as $row)
                    <tr>
                        <td>{{ $row->title }}</td>
                        <td>
                            <a class="btn btn-default btn-xs" href="{{ route('beer-types.edit', ['type' => $row]) }}">Edit</a>

                            <form method="POST" action="{{ route('beer-types.destroy', ['type' => $row]) }}" class="confirm inline-block">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
