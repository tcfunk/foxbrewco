@extends('layouts.admin')

@section('title', $type->title)

@section('breadcrumb')
    <li><a href="{{ route('beer_types.index') }}">Beer Types</a></li>
    <li>{{ $type->title }}</li>
@endsection

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('beer_types.update', ['type' => $type]) }}">
            {{ method_field('put') }}
            @include('admin.beer_types._fields')
            <button class="btn btn-default">Update</button>
        </form>
    </div>
@endsection
