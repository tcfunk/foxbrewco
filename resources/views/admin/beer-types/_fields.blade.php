{{ csrf_field() }}

<div class="row">
    <div class="form-group col-sm-6">
        <label for="title">Title</label>
        <input class="form-control" id="title" name="title" type="text" value="{{ $type->title }}"/>
    </div>
</div>

