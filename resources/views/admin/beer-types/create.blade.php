@extends('layouts.admin')

@section('title', 'New Beer Type')

@section('breadcrumb')
    <li><a href="{{ route('beer-types.index') }}">Beer Types</a></li>
    <li>New Beer Type</li>
@endsection

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('beer-types.store') }}">
            @include('admin.beer-types._fields')
            <button class="btn btn-default">Create</button>
        </form>
    </div>
@endsection
