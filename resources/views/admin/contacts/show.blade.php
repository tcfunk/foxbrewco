@extends('layouts.admin')

@section('title', $contact->name)

@section('breadcrumb')
    <li><a href="{{ action('AdminContactsController@index') }}">Contact Requests</a></li>
    <li>{{ $contact->name }}</li>
@endsection

@section('content')
    <div class="container">
        <dl>
            <dt>Name</dt>
            <dd>{{ $contact->name }}</dd>

            <dt>Email</dt>
            <dd>{{ $contact->email }}</dd>

            <dt>Phone</dt>
            <dd>{{ $contact->phone }}</dd>

            <dt>Message</dt>
            <dd>{!! preg_replace('/\n/', '<br>', htmlentities($contact->message)) !!}</dd>
        </dl>
    </div>
@endsection
