@extends('layouts.admin')

@section('title', 'Contact Requests')

@section('breadcrumb')
<li>Contact Requests</li>
@endsection

@section('content')
    <div class="container">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th width="35%">Name</th>
                    <th width="30%">Email</th>
                    <th width="20%">Phone</th>
                    <th width="15%">Manage</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contacts as $row)
                    <tr>
                        <td>{{ $row->name }}</td>
			                  <td>{{ $row->email }}</td>
			                  <td>{{ $row->phone }}</td>
                        <td>
                            <a href="{{ action('AdminContactsController@show', ['contact' => $row]) }}" class="btn btn-default btn-xs">View</a>

                            <form method="POST" action="{{ action('AdminContactsController@destroy', ['contact' => $row]) }}" class="confirm inline-block">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
		                </tr>
	              @endforeach
	          </tbody>
        </table>
    </div>
@endsection
