@extends('layouts.admin')

@section('title', $article->title)

@section('breadcrumb')
    <li><a href="{{ route('articles.index') }}">Articles</a></li>
    <li>{{ $article->title }}</li>
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('articles.update', $article) }}" enctype="multipart/form-data">
        <div class="row">
            {{ method_field('put') }}

            <div class="col-sm-8">
                @include('admin.articles._fields')
                <button class="btn btn-default">Update</button>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Image (900px &times; 500px)</label>
                    <input type="file" name="photo" id="photo" />
                </div>

                @if(!empty($article->photo))
                    <div class="form-group">
                        <img id="img-crop" src="{{ asset(Storage::url($article->photo)) }}" alt="croppable" class="img-responsive" />
				            </div>
                @endif
            </div>

        </div>

    </form>
</div>
@endsection
