@extends('layouts.admin')

@section('title', 'New Article')

@section('breadcrumb')
    <li><a href="{{ route('articles.index') }}">Articles</a></li>
    <li>Articles</li>
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('articles.store') }}" enctype="multipart/form-data">
        <div class="row">

            <div class="col-sm-8">
                @include('admin.articles._fields')
                <button class="btn btn-default">Create</button>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="image">Image (900px &times; 500px)</label>
                    <input type="file" name="photo" id="photo" />
                </div>
            </div>

        </div>

    </form>
</div>
@endsection
