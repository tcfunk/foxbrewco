{{ csrf_field() }}

<div class="row">
    <div class="form-group col-sm-8">
        <label for="title">Title</label>
        <input id="title" name="title" type="text" value="{{ $article->title }}" class="form-control"/>
    </div>

    <div class="form-group col-sm-4">
        <label for="date">Date</label>
        <input id="date" name="date" type="date" value="{{ $article->date?$article->date->format('Y-m-d'):'' }}" class="datepicker form-control"/>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12">
        <label for="body">Article Body</label>
        <input id="body" name="body" type="hidden" value="{{ $article->body }}"/>
        <div id="toolbar"></div>
        <div id="quill-editor" data-field="body">{!! $article->body !!}</div>
    </div>
</div>
