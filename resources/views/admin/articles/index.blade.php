@extends('layouts.admin')

@section('title', 'News Articles')

@section('breadcrumb')
    <li>News Articles</li>
@endsection

@section('content')
    <div class="container">

        <a class="btn btn-success pull-right" href="{{ route('articles.create') }}"><i class="glyphicon glyphicon-plus"></i> Add Article</a>

        <table class="table table-hover table-striped">
            <thead>
                <th width="15%">Date</th>
                <th>Title</th>
                <th width="15%">Manage</th>
            </thead>
            <tbody>
                @foreach ($articles as $row)
                    <tr>
                        <td>{{ $row->date->format('M j, Y') }}</td>
                        <td>{{ $row->title }}</td>
                        <td>
                            <a class="btn btn-default btn-xs" href="{{ route('articles.edit', ['type' => $row]) }}">Edit</a>

                            <form method="POST" action="{{ route('articles.destroy', ['type' => $row]) }}" class="confirm inline-block">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
