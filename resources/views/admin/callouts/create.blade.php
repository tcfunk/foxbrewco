@extends('layouts.admin')

@section('title', 'New Callout')

@section('breadcrumb')
    <li><a href="{{ route('callouts.index') }}">Callouts</a></li>
    <li>New Callout</li>
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('callouts.store') }}" enctype="multipart/form-data">
        <div class="row">

            <div class="col-sm-12">
                @include('admin.callouts._fields')
            </div>

            <div class="col-sm-12">
                <button class="btn btn-default">Create</button>
            </div>

        </div>

    </form>
</div>
@endsection
