{{ csrf_field() }}


<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>
                <input name="active" type="checkbox" value="1" {{ $callout->active ? 'checked=checked':'' }}/>
                Active
            </label>
        </div>

        <div class="form-group">
            <label for="image">Image (800px &times; 600px)</label>
            <input type="file" name="image" id="image" />
            @if(!empty($callout->image))
                <img class="img-responsive" alt="" src="{{ asset(Storage::url($callout->image)) }}"/>
            @endif
        </div>
    </div>
</div>


<div class="row">
    <div class="form-group col-sm-6">
        <label for="title">Overlay text <span class="required">*</span></label>
        <input id="title" name="title" type="text" value="{{ $callout->title }}" class="form-control"/>
    </div>

    <div class="form-group col-sm-6">
        <label for="link">Link <span class="required">*</span></label>
        <input id="link" class="form-control" name="link" type="text" value="{{ $callout->link }}"/>
    </div>

    <div class="form-group col-sm-4">
        <label for="icon">Icon</label>
        <select id="icon" class="form-control" name="icon">
            <option>No Icon</option>
            @foreach($icons as $class => $title)
                <option value="{{ $class }}" {{ $class==$callout->icon?'selected=selected':'' }}>{{ $title }}</option>
            @endforeach
        </select>
        <span class="help-block"><i id="icon-preview"></i></span>
    </div>
</div>
