@extends('layouts.admin')

@section('title', $callout->title)

@section('breadcrumb')
    <li><a href="{{ route('callouts.index') }}">Callouts</a></li>
    <li>{{ $callout->title }}</li>
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('callouts.update', $callout) }}" enctype="multipart/form-data">
        {{ method_field('PUT') }}

        <div class="row">
            <div class="col-sm-12">
                @include('admin.callouts._fields')
            </div>

            <div class="col-sm-12">
                <button class="btn btn-default">Update</button>
            </div>
        </div>
    </form>
</div>
@endsection
