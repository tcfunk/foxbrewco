@extends('layouts.admin')

@section('title', 'Homepage Callouts')

@section('breadcrumb')
    <li>Callouts</li>
@endsection

@section('content')
    <div class="container">

        <a class="btn btn-success pull-right" href="{{ route('callouts.create') }}"><i class="glyphicon glyphicon-plus"></i> Add Callout</a>

        <table class="table table-hover table-striped">
            <colgroup>
                <col class="title"/>
                <col class="manage"/>
            </colgroup>

            <thead>
                <th>Title</th>
                <th width="15%">Manage</th>
            </thead>

            <tbody>
                @foreach ($callouts as $row)
                    <tr>
                        <td>{{ $row->title }}</td>
                        <td>
                            <a class="btn btn-default btn-xs" href="{{ route('callouts.edit', ['type' => $row]) }}">Edit</a>

                            <form method="POST" action="{{ route('callouts.destroy', ['type' => $row]) }}" class="confirm inline-block">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
