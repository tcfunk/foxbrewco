<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{ url('/admin') }}">Fox Brewing</a>
        </div>

        <div class="collapse navbar-collapse" id="admin-navbar">
            <ul class="nav navbar-nav navbar-right">

                @if(auth()->user())
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Homepage <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('slides.index') }}">Slides</a></li>
                        <li><a href="{{ route('callouts.index') }}">Callouts</a></li>
                    </ul>
                </li>

                <li><a href="{{ route('beers.index') }}">Beers</a></li>
                <li><a href="{{ route('events.index') }}">Events</a></li>
                <li><a href="{{ route('articles.index') }}">News</a></li>
                <li><a href="{{ action('Admin\ContactsController@index') }}">Contact Requests</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-lock"></i> {{ auth()->user()->name }} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><form action="{{ route('logout') }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-link" href="{{ route('logout') }}">Logout</button>
                        </form></li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>

    </div>
</nav>
