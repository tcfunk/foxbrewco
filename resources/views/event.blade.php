@extends('layouts.main')

@section('title', $event->title)

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li><a href="{{ url('events') }}">Events</a></li>
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>{{ $event->title }}</li>
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                    @if(!empty($event->photo))
                        <img src="{{ asset(Storage::url($event->photo)) }}" alt="Event photo" class="img-responsive" />
                    @else
                        <img alt="event image" src="http://placehold.it/900x675" class="img-responsive"/>
                    @endif
            </div>

            <div class="card col-sm-6 only">
                <h1>{{ $event->title }}</h1>

                <p>
                    <i class="glyphicon glyphicon-time"></i>
                    {{ $event->date->format('F j, Y') }}
                    {{ $event->startTime() }} &ndash; {{ $event->endTime() }}
                </p>

                <p>
                    <i class="glyphicon glyphicon-map-marker"></i>
                    <a target="_blank" href="{{ $event->addressLink() }}">{{ $event->venue() }}</a>
                </p>

                <b>Price:</b> {{ $event->price }}
                <hr/>
                {!! $event->body !!}
            </div>

        </div>
    </div>
@endsection
