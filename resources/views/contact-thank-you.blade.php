@extends('layouts.main')

@section('title', 'Contact')

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li><a href="/contact">Contact</a></li>
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>Thank You</li>
@endsection

@section('content')
    <div class="container">
        <div class="card col-md-8 col-sm-12">
            <h1>Thank You</h1>
            <hr/>

            <p>
                Thank you for reaching out to us. We will get back to you asap.
            </p>
        </div>

        @include('shared.contact-sidenav')
    </div>
@endsection
