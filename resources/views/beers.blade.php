@extends('layouts.main')

@section('title', 'Beers')

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>Beers</li>
@endsection

@section('content')
    <div id="beers-container" class="container">

        <ul class="nav nav-pills">
            <li role="presentation" class="active"><a href="">All</a></li>
            <li role="presentation"><a href="">Seasonal</a></li>
            <li role="presentation"><a href="">Year-Round</a></li>
        </ul>

        <div id="beer-list">
        @foreach ($beers as $beer)
            <div class="card col-md-4 col-sm-6">
                <figure>
                    @if(!empty($beer->image))
                        <img alt="{{ $beer->title }}" src="{{ asset(Storage::url($beer->image)) }}"/>
                    @else
                        <img alt="{{ $beer->title }}" src="http://placehold.it/800x600" />
                    @endif
                </figure>

                <h3>{{ $beer->title }} <small>{{ $beer->beer_type }}</small></h3>
                <p>ABV: {{ $beer->abv }} IBU: {{ $beer->ibu }}</p>
                <hr/>
                <p>{{ $beer->description }}</p>
            </div>
        @endforeach
        </div>

    </div>
@endsection

@section('scripts')
	<script src="/js/beers.js"></script>
@endsection
