<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title') | Fox Brewing</title>
        <link href="/css/app.css" rel="stylesheet"/>
    </head>
    <body>
			<header>
        @include('shared.nav')

				<div class="title-section">
            <div class="container">
                <h1>@yield('title')</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ url('') }}"><i class="glyphicon glyphicon-home"></i></a>
                    </li>
                    @yield('breadcrumb')
                </ol>
            </div>
        </div>

			</header>

        <section class="main">
            @yield('content')
        </section>

        @include('shared.triptych')
        @include('shared.footer')

        <script>
         window.Laravel = { csrfToken: '{{ csrf_token() }}' };
        </script>
        <script src="/js/app.js"></script>
				@yield('scripts');
    </body>
</html>
