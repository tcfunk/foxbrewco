<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title') | Admin - Fox Brewing</title>
        <link href="/css/admin.css" rel="stylesheet"/>
		@yield('css')
    </head>
    <body>
        @include('admin.shared.nav')

        @if(auth()->user())
        <div class="title-section">
            <div class="container">
                <h1>@yield('title')</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/admin') }}">Admin Home</a></li>
                    @yield('breadcrumb')
                </ol>
            </div>
        </div>
        @endif

        <section class="main">
            @yield('content')
        </section>

        <script>
         window.Laravel = { csrfToken: '{{ csrf_token() }}' };
        </script>
        <script src="/js/admin.js"></script>
		@yield('js')
    </body>
</html>

