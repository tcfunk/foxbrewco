@extends('layouts.main')

@section('title', $article->title)

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li><a href="{{ url('news') }}">News</a></li>
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>{{ $article->title }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">

                    <div class="card news-card">

                        @if($article->photo)
                        <img class="img-responsive" alt="event image" src="{{ asset(Storage::url($article->photo)) }}"/>
                        @endif
                        <h2>{{ $article->title }}</h2>

                        <p class="well well-sm">Posted {{ $article->date->format('F j, Y') }}</p>

                        {!! $article->body !!}

                        <nav class="article-nav nav">
                            @if($prev)
                            <a class="prev" href="{{ url('news/'.$prev->slug) }}" rel="prev"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp;Previous&nbsp;Post</a>
                            @endif

                            @if($next)
                            <a class="next" href="{{ url('news/'.$next->slug) }}" rel="next">Next&nbsp;Post&nbsp;<i class="glyphicon glyphicon-arrow-right"></i></a>
                            @endif
                        </nav>
                    </div>
            </div>

            @if($years)
            <div class="col-sm-4">
                <div class="card">
                    <h3>Archives</h3>
                    <hr/>
                    <ul>
                        @foreach($years as $year)
                            <li><a href="{{ url('news/archives/'.$year->date_part) }}">{{ $year->date_part }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
        </div>

    </div>
@endsection
