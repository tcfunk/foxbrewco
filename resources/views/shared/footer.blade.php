<footer>
    <nav class="navbar navbar-inverse">
        <div class="container">

            <ul class="nav navbar-nav navbar-left">
                <li class="copyright">&copy; {{ date('Y') }} Fox Brewing Company</li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ url('') }}">Home</a>
                </li>

                <li>
                    <a href="{{ url('beers') }}">Beers</a>
                </li>

                <li>
                    <a class="current" href="{{ url('events') }}">Events</a>
                </li>

                <li>
                    <a href="{{ url('contact') }}">Contact</a>
                </li>
            </ul>

        </div>
    </nav>
</footer>
