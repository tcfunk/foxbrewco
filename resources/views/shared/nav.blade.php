<nav class="navbar navbar-inverse main-nav navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('img/logo.png') }}" alt="Fox Brewing Company" />
            </a>
        </div>

        <div id="navbar-collapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a {{ url()->current()==url('')?'class=current':'' }} href="{{ url('') }}">Home</a>
                </li>

                <li>
                    <a {{ url()->current()==url('beers')?'class=current':'' }} href="{{ url('beers') }}">Beers</a>
                </li>

                <li>
                    <a {{ url()->current()==url('taproom')?'class=current':'' }} href="{{ url('taproom') }}">Taproom</a>
                </li>

                <li>
                    <a {{ url()->current()==url('events')?'class=current':'' }} href="{{ url('events') }}">Events</a>
                </li>

                <li>
                    <a {{ url()->current()==url('news')?'class=current':'' }} href="{{ url('news') }}">News</a>
                </li>

                <li>
                    <a {{ url()->current()==url('contact')?'class=current':'' }} href="{{ url('contact') }}">Contact</a>
                </li>
            </ul>
        </div>

    </div>
</nav>


