<div class="col-md-4 col-sm-12 no-pad-sm">

    <div class="card col-md-12 col-sm-6 col-xs-12">
        <h3>Contact Info</h3>
        <hr/>
        <address>
            <strong>Fox Brewing Company</strong><br>
            103 S 11th Street<br>
            West Des Moines, IA 50265<br>
        </address>
        <address>
            <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> (515) 635-0323<br>
            <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i> <a href="mailto:info@foxbrewco.com">info@foxbrewco.com</a>
        </address>
    </div>

    <div class="card col-md-12 col-sm-6 col-xs-12">
        <h3>Taproom Hours</h3>
        <hr/>
        <table class="table taproom-hours">
            <tbody>
                <tr>
                    <th>Monday</th>
                    <td>8:00 AM - 11:00 PM</td>
                </tr>
                <tr>
                    <th>Tuesday</th>
                    <td>8:00 AM - 11:00 PM</td>
                </tr>
                <tr>
                    <th>Wednesday</th>
                    <td>8:00 AM - 11:00 PM</td>
                </tr>
                <tr>
                    <th>Thursday</th>
                    <td>8:00 AM - 11:00 PM</td>
                </tr>
                <tr>
                    <th>Friday</th>
                    <td>8:00 AM - 11:00 PM</td>
                </tr>
                <tr>
                    <th>Saturday</th>
                    <td>8:00 AM - 11:00 PM</td>
                </tr>
                <tr>
                    <th>Sunday</th>
                    <td>8:00 AM - 11:00 PM</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
