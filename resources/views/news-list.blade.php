@foreach($articles as $article)
    <div class="card news-card">

        @if($article->photo)
            <img class="img-responsive" alt="event image" src="{{ asset(Storage::url($article->photo)) }}"/>
        @endif

        <h2>{{ $article->title }}</h2>

        <p class="well well-sm">Posted {{ $article->date->format('F j, Y') }}</p>

        {!! $article->body !!}

        <p>
            <a href="{{ url('news/'.$article->slug) }}">Continue reading <i class="glyphicon glyphicon-arrow-right"></i></a>
        </p>
    </div>
@endforeach
