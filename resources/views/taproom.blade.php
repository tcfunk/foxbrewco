@extends('layouts.main')

@section('title', 'Taproom')

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>Taproom</li>
@endsection


@section('content')
    <div class="container">
        <div class="card col-md-8 col-sm-12 editor-content">
            <h1>About Our Taproom</h1>

            <figure>
                <img alt="" src="{{ asset('img/800x600.png') }}"/>
                <figcaption>Image caption</figcaption>
            </figure>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis neque dui, at vulputate enim fringilla sed. Phasellus felis augue, consectetur vitae magna at, auctor facilisis ipsum. Fusce feugiat sapien nec semper semper. Phasellus dictum tellus in leo eleifend maximus. Vestibulum blandit, lacus blandit porta euismod, lacus enim ornare nisl, eget dapibus nibh nulla eu lorem. Mauris accumsan magna a nibh consequat ultricies. Sed luctus vestibulum nibh, eu malesuada nibh elementum vitae. Ut congue molestie tincidunt. Sed quis augue nec sem dictum tempor at vel lorem. Morbi vitae risus tempus, placerat urna quis, mollis dui. Proin sed arcu quis enim porttitor dignissim sit amet at felis. In sollicitudin odio massa, vel accumsan nisi dignissim sed. Nunc in luctus diam, nec pharetra purus. Nullam blandit mi vel tristique pretium. Integer ultrices sem at nibh imperdiet, in aliquet leo hendrerit. Cras mi enim, gravida eu lectus vel, suscipit auctor augue.
            </p>
            <blockquote>
                Maecenas a ornare turpis. In vitae gravida risus, quis auctor est. Sed elementum augue nibh, ut auctor augue consequat id. Ut tristique, augue eget imperdiet scelerisque, elit tellus tempor massa, sed convallis urna nisi sed ligula. Aliquam odio lacus, rhoncus eget tortor eget, tincidunt elementum felis. In tristique orci lectus, quis ultricies risus pharetra at. Praesent ut felis feugiat, accumsan sem et, rhoncus quam.
            </blockquote>
            <p>
                Duis tempus, metus id bibendum dictum, sem lacus ultrices nibh, bibendum gravida leo tortor a magna. Morbi at ornare sapien. In hac habitasse platea dictumst. Nullam in purus a dui porta tempor. Sed at viverra odio, convallis pulvinar mi. Nullam et urna mauris. Vestibulum eu fermentum nunc, quis posuere eros. Suspendisse viverra elementum nibh a lobortis. Cras rutrum felis in mi ultricies, non tempus nunc imperdiet. Morbi augue risus, lacinia in diam id, mollis cursus nibh. Morbi lacinia a lacus et imperdiet. Vivamus ut orci tortor. Aenean nec efficitur sapien. Quisque eu elit nec lectus blandit posuere eu sed orci. Nullam ullamcorper, urna eu volutpat sodales, ex ipsum efficitur justo, a placerat odio nisl vel libero. Phasellus aliquam posuere odio, auctor vehicula arcu ultricies vel.
                </p>
                <p>
                    In quis velit fringilla dolor hendrerit aliquet. Cras sed tempus velit. Morbi finibus varius urna, vel ornare sem venenatis nec. Pellentesque sed urna nec ex scelerisque rhoncus in vel purus. Suspendisse potenti. Duis porttitor, dolor a ullamcorper tincidunt, velit tellus blandit ante, eu rutrum est lacus et augue. Suspendisse accumsan nisi sit amet congue auctor. Mauris faucibus risus at sem fringilla, a elementum libero viverra. Phasellus ac hendrerit sem. Maecenas sodales odio massa, in ultrices purus tincidunt eget. Sed gravida egestas commodo. Proin at nibh vitae lorem consequat pretium. Nam molestie turpis volutpat tempus varius.
                    </p>
                    <p>
                        Maecenas euismod purus a neque venenatis, nec aliquam dui iaculis. Pellentesque pulvinar dui ac nisl porta, a dictum mauris aliquet. Aenean interdum ex risus, ut pellentesque libero ultrices quis. Vivamus congue, lacus et malesuada pulvinar, sapien justo congue tellus, vel condimentum nisi arcu in arcu. Nulla elit sapien, cursus sit amet iaculis eu, blandit condimentum justo. Integer dignissim, nisi in dignissim ullamcorper, metus metus pharetra urna, sit amet posuere erat velit non augue. Aenean et arcu vel metus gravida tincidunt. Suspendisse pharetra consequat scelerisque. Phasellus convallis vehicula ligula eget rhoncus. Fusce et quam auctor, dictum dolor non, fermentum nisl. Sed at ipsum vel lacus semper porta. Aenean auctor lectus lectus, nec pretium est dapibus a.
                        </p>
        </div>

        @include('shared.contact-sidenav')
    </div>
@endsection
