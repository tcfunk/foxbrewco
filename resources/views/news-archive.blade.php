@extends('layouts.main')

@section('title', 'News - '.$year)

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li><a href="{{ url('news') }}">News</a></li>
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>{{ $year }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-sm-8">
                @include('news-list')
            </div>

            <div class="col-sm-4">
                @if($years)
                    <div class="card">
                        <h3>Archives</h3>
                        <hr/>
                        <ul>
                            @foreach($years as $year)
                                <li><a href="{{ url('news/archives/'.$year->date_part) }}">{{ $year->date_part }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        @if($articles->currentPage() != 1 || $articles->hasMorePages())
        <div class="row">
            <div class="col-xs-12">
                <nav>
                    <ul class="pager">
                        @if($articles->currentPage() == 1)
                            <li class="disabled">
                                <span><i class="glyphicon glyphicon-circle-arrow-left"></i> Previous</span>
                            </li>
                        @else
                            <li>
                                <a href="{{ $articles->previousPageUrl() }}"><i class="glyphicon glyphicon-circle-arrow-left"></i> Previous</a>
                            </li>
                        @endif

                        @if($articles->hasMorePages())
                            <li>
                                <a href="{{ $articles->nextPageUrl() }}">More <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
                            </li>
                        @else
                            <li class="disabled">
                                <span>More <i class="glyphicon glyphicon-circle-arrow-right"></i></span>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
        @endif

    </div>
@endsection
