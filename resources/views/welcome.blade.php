<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Fox Brewing</title>
    <link href="/css/app.css" rel="stylesheet"/>
</head>
<body>
	<header>
    @include('shared.nav')

		<div class="title-section">
			<div class="container">
				<h1>@yield('title')</h1>
				<ol class="breadcrumb">
					<li>
						<a href="{{ url('') }}"><i class="glyphicon glyphicon-home"></i></a>
					</li>
					@yield('breadcrumb')
				</ol>
			</div>
		</div>
	</header>

		<section class="callouts">
				<div class="container">

            @foreach($callouts as $callout)
                <div class="col-sm-4">
                    <a href="{{ $callout->link }}">
                        <figure>
                            <img alt="callout image" src="{{ asset(Storage::url($callout->image)) }}"/>
                            <figcaption>
                                @if(!empty($callout->icon))
                                <i class="glyphicon {{ $callout->icon }}"></i>
                                @endif
                                <h3>{{ $callout->title }}</h3>
                            </figcaption>
                        </figure>
                    </a>
                </div>
            @endforeach

        </div>
    </section>

    <section class="main">

        <div class="container">

            <h1>Featured Beers</h1>

            <div>
            @foreach($beers as $beer)
                <div class="card col-md-4 col-sm-6">
                    <figure>
                        <img alt="{{ $beer->title }}" src="{{ asset(Storage::url($beer->image)) }}"/>
                    </figure>

                    <h3>{{ $beer->title }} <small>{{ $beer->beer_type }}</small></h3>
                    <p>ABV: {{ $beer->abv }} IBU: {{ $beer->ibu }}</p>

                    <hr/>
                    <p>{{ $beer->description }}</p>
                </div>
            @endforeach
            </div>

            <div class="col-sm-12">
                <p class="lead">
                    <a href="{{ url('beers') }}">See all beers &rarr;</a>
                </p>
            </div>

        </div>

    </section>

    <section class="events">
        <div class="container">
            <h1>Upcoming Events</h1>

            @foreach($events as $event)
                <div class="card event-card col-md-3 col-sm-6">
                    <figure>
                        <img alt="event image" src="{{ asset(Storage::url($event->photo)) }}"/>
                    </figure>

                    <h3>{{ $event->title }}</h3>

                    <p>
                        <i class="glyphicon glyphicon-time"></i>
                        {{ $event->date->format('F j, Y') }}
                        {{ $event->startTime() }} &ndash; {{ $event->endTime() }}
                    </p>

                    <p>
                        <i class="glyphicon glyphicon-map-marker"></i>
                        <a target="_blank" href="{{ $event->addressLink() }}">{{ $event->venue() }}</a>
                    </p>

                    <hr/>

                    <p><a class="btn btn-primary btn-md" href="/events/{{ $event->slug }}">View Event <i class="glyphicon glyphicon-circle-arrow-right"></i></a></p>
                </div>
            @endforeach

            <div class="col-sm-12">
                <p class="lead">
                    <a href="{{ url('events') }}">See all events &rarr;</a>
                </p>
            </div>

        </div>
    </section>

    @include('shared.triptych')
    @include('shared.footer')

    <script>
        window.Laravel = { csrfToken: '{{ csrf_token() }}' };
    </script>
    <script src="/js/app.js"></script>
    <script>jQuery = $;</script>
    <script src="/js/backstretch.js"></script>
    <script>
     (function() {
        $(function() {
            function applyBackstretch() {
                $('.carousel .item.active').each(function() {
                    var src = $(this).data('src');

                    $(this).backstretch(src, {
                        alignX: 'center',
                        alignX: 'center'
                    });
                });
            }

            $('#hero-carousel').on('slid.bs.carousel', applyBackstretch);
            setTimeout(applyBackstretch, 0);
        });
     })();
    </script>
</body>
</html>
