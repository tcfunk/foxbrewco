@extends('layouts.main')

@section('title', 'Events')

@section('breadcrumb')
    <li class="separator"><i class="glyphicon glyphicon-chevron-right"></i></li>
    <li>Events</li>
@endsection


@section('content')
    <div class="container">

        @foreach($events as $event)
            <div class="card event-card col-md-4 col-sm-6">
                <figure>
                    @if(!empty($event->photo))
                        <img alt="event image" src="{{ asset(Storage::url($event->photo)) }}"/>
                    @else
                        <img alt="event image" src="http://placehold.it/900x675"/>
                    @endif
                </figure>

                <h3>{{ $event->title }}</h3>

                <p>
                    <i class="glyphicon glyphicon-time"></i>
                    {{ $event->date->format('F j, Y') }}
                    {{ $event->startTime() }} &ndash; {{ $event->endTime() }}
                </p>

                <p>
                    <i class="glyphicon glyphicon-map-marker"></i>
                    <a target="_blank" href="{{ $event->addressLink() }}">{{ $event->venue() }}</a>
                </p>

                <hr/>

                <p><a class="btn btn-primary btn-md" href="/events/{{ $event->slug }}">View Event <i class="glyphicon glyphicon-circle-arrow-right"></i></a></p>
            </div>
        @endforeach

        @if($events->currentPage() != 1 || $events->hasMorePages())
        <div class="row">
            <div class="col-xs-12">
                <nav>
                    <ul class="pager">
                        @if($events->currentPage() == 1)
                            <li class="disabled">
                                <span><i class="glyphicon glyphicon-circle-arrow-left"></i> Previous</span>
                            </li>
                        @else
                            <li>
                                <a href="{{ $events->previousPageUrl() }}"><i class="glyphicon glyphicon-circle-arrow-left"></i> Previous</a>
                            </li>
                        @endif

                        @if($events->hasMorePages())
                            <li>
                                <a href="{{ $events->nextPageUrl() }}">More <i class="glyphicon glyphicon-circle-arrow-right"></i></a>
                            </li>
                        @else
                            <li class="disabled">
                                <span>More <i class="glyphicon glyphicon-circle-arrow-right"></i></span>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
        @endif

    </div>
@endsection
