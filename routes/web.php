<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \App\Http\Requests\ContactRequest;
use \App\Contact;
use \App\Beer;
use \App\Event;
use \App\Article;
use \App\Slide;
use \App\Callout;


Auth::routes();

Route::get('/', function () {
    $slides = Slide::where('active', true)
            ->orderBy('weight')
            ->limit(5)
            ->get();
    $callouts = Callout::where('active', true)
              ->orderBy('weight', 'id')
              ->limit(3)
              ->get();
    $beers = Beer::limit(3)->get();
    $events = Event::where('date', '>', DB::raw('current_date - 1'))
            ->orderBy('date', 'asc')
            ->limit(4)
            ->get();
    return view('welcome', compact('slides', 'callouts', 'beers', 'events'));
});

Route::get('/beers', function() {
    $beers = Beer::orderBy('title')->get();
    return view('beers', compact('beers'));
});

Route::get('/taproom', function() {
    return view('taproom');
});

Route::get('/events', function() {
    $events = Event::where('date', '>', DB::raw('current_date - 1'))
            ->orderBy('date', 'asc')
            ->simplePaginate(9);
    return view('events', compact('events'));
});

Route::get('/events/{slug}', function($slug) {
    $event = Event::where('slug', $slug)->first();
    if (empty($event)) {
        return redirect('/events');
    }
    return view('event', compact('event'));
});

Route::get('/news', function() {
    $articles = Article::where('date', '<=', DB::raw('current_date'))
        ->orderBy('date', 'desc')
        ->simplePaginate(4);

    $years = DB::table('articles')
           ->select(DB::raw('extract(year from date)'))
           ->distinct()
           ->get();

    return view('news', compact('articles', 'years'));
});

Route::get('/news/{slug}', function($slug) {
    $article = Article::where('slug', $slug)->first();
    $next = Article::where('date', '>', $article->date)->first();
    $prev = Article::where('date', '<', $article->date)->first();
    $years = DB::table('articles')
           ->select(DB::raw('extract(year from date)'))
           ->distinct()
           ->get();

    if (empty($article)) {
        return redirect('news');
    }

    return view('article', compact('slug', 'article', 'next', 'prev', 'years'));
});

Route::get('/news/archives/{year}', function($year) {
    $articles = Article::where(DB::raw('extract(year from date)'), '=', $year)
              ->orderBy('date', 'desc')
              ->simplePaginate(4);

    $years = DB::table('articles')
           ->select(DB::raw('extract(year from date)'))
           ->distinct()
           ->get();

    return view('news-archive', compact('articles', 'years', 'year'));
});

Route::get('/contact', function() {
    return view('contact');
});

Route::get('/contact/thank-you', function() {
	return view('contact-thank-you');
});

Route::post('/contact', function(ContactRequest $request) {
    // If any of this code happens, it means ContactRequest
    // validated correctly

    // Store the request in the database
	Contact::create(request(['name', 'phone', 'email', 'message']));

    // Redirect to thank-you page
	return redirect('/contact/thank-you');
});


// ----------------------------------------------------------------
// Admin Routes
// ----------------------------------------------------------------

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('', 'Controller@index');
    Route::resource('slides', 'SlidesController');
    Route::resource('callouts', 'CalloutsController');
    Route::resource('beers', 'BeersController');
    Route::resource('beer-types', 'BeerTypesController');
    Route::resource('events', 'EventsController');
    Route::resource('articles', 'NewsController');

    // Contact Requests
    Route::get('contacts', 'ContactsController@index');
    Route::get('contacts/{contact}', 'ContactsController@show');
    Route::delete('contacts/{contact}', 'ContactsController@destroy');

	// Weird URL due to conflict with BeerController@update
	Route::post('/upload/beers', 'BeersController@upload');
	// Route::post('/upload/beers', function() {
	// 	return response()->json(['testing' => '123']);
	// });
});


// ----------------------------------------------------------------
// JSOn API Routes
// ----------------------------------------------------------------

Route::group(['prefix' => 'api', 'namespace' => 'API'], function() {
	Route::get('beers', 'BeersController@index');
});